import React from 'react';
import {Redirect, Route} from 'react-router-dom';
import {IonApp} from '@ionic/react';
import {IonReactRouter} from '@ionic/react-router';
/* Core CSS required for Ionic components to work properly */
import '@ionic/react/css/core.css';
/* Basic CSS for apps built with Ionic */
import '@ionic/react/css/normalize.css';
import '@ionic/react/css/structure.css';
import '@ionic/react/css/typography.css';
/* Optional CSS utils that can be commented out */
import '@ionic/react/css/padding.css';
import '@ionic/react/css/float-elements.css';
import '@ionic/react/css/text-alignment.css';
import '@ionic/react/css/text-transformation.css';
import '@ionic/react/css/flex-utils.css';
import '@ionic/react/css/display.css';
/* Theme variables */
import './theme/variables.css';
import WelcomePage from "./pages/WelcomePage";
import LoginPage from "./pages/LoginPage";
import HomePage from "./pages/HomePage";
import BookEventPage from "./pages/EventBookingPage";
import ChooseActivityPage from "./pages/ChooseActivityPage";
import ChooseVadhyarPage from "./pages/ChooseVadhyarPage";
import ChooseCatererPage from "./pages/ChooseCatererPage";
import ChoosePlacePage from "./pages/ChoosePlacePage";
import ConfirmationPage from "./pages/ConfirmationPage";

const App: React.FC = () => (
    <IonApp>
        <IonReactRouter>
            <Route path="/welcome" component={WelcomePage} exact={true}/>
            <Route exact path="/" render={() => <Redirect to="/welcome"/>}/>
            <Route path="/login" component={LoginPage} exact={true}/>
            <Route path="/login/:isLogin" render={props => <LoginPage {...props} />} exact={true}/>
            <Route path="/home" component={HomePage} exact={true}/>
            <Route path="/book-event" component={BookEventPage} exact={true}/>
            <Route path="/choose-activity" component={ChooseActivityPage} exact={true}/>
            <Route path="/choose-vadhyar" component={ChooseVadhyarPage} exact={true}/>
            <Route path="/choose-vadhyar/:isBackHome" render={props => <ChooseVadhyarPage {...props} />} exact={true}/>
            <Route path="/choose-caterer" component={ChooseCatererPage} exact={true}/>
            <Route path="/choose-caterer/:isBackHome" render={props => <ChooseCatererPage {...props} />} exact={true}/>
            <Route path="/choose-place" component={ChoosePlacePage} exact={true}/>
            <Route path="/choose-place/:isBackHome" render={props => <ChoosePlacePage {...props} />} exact={true}/>
            <Route path="/confirm-booking" component={ConfirmationPage} exact={true}/>
        </IonReactRouter>
    </IonApp>
);

export default App;
