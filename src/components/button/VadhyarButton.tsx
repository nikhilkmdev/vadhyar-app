import React from 'react';
import './VadhyarButton.css';
import {IonButton, IonRippleEffect} from "@ionic/react";

interface ContainerProps {
    expand?: any;
    onClick?: any;
    name: string;
    size?: any;
}

const VadhyarButton: React.FC<ContainerProps> = ({expand, name, size, onClick}) => {
    return (
        <IonButton
            color="vadhyar"
            size={size}
            expand={expand}
            shape="round"
            onClick={onClick}>
            {name}
            <IonRippleEffect type="unbounded"></IonRippleEffect>
        </IonButton>
    );
};

export default VadhyarButton;
