import React from 'react';
import './VadhyarItemBookPlace.css';
import {IonButton, IonCard, IonCardContent, IonCol, IonIcon, IonRow} from "@ionic/react";
import {add, star} from "ionicons/icons";

interface ContainerProps {
    index: number;
    name: string;
    bio: string;
    cost: string;
    rating: number;
}

const VadhyarItemBookPlace: React.FC<ContainerProps> = ({index, name, bio, cost, rating}) => {

    return (
        <IonRow className="vadhyar-item-choose-place-row">
            <IonCol sizeLg="10">
                <IonCard className="vadhyar-item-choose-place-card">
                    <IonCardContent className="vadhyar-item-choose-place-card-content">
                        <IonRow>
                            <IonCol size="2" sizeSm="2" sizeLg="2"
                                    className="vadhyar-item-choose-place-index-col">
                                <div className="vadhyar-item-choose-place-index-text">{index}</div>
                            </IonCol>
                            <IonCol size="7" sizeSm="7" sizeLg="7">
                                <IonRow>
                                    <IonCol>
                                        <div className="vadhyar-item-choose-place-name">{name}</div>
                                    </IonCol>
                                </IonRow>
                                <IonRow>
                                    <IonCol>
                                        <div className="vadhyar-item-choose-place-bio">{bio}</div>
                                    </IonCol>
                                </IonRow>
                                <IonRow>
                                    <IonCol>
                                        <div
                                            className="vadhyar-item-choose-place-experience">{cost}
                                        </div>
                                    </IonCol>
                                    <IonCol>
                                        <div className="vadhyar-item-choose-place-rating">
                                            <IonIcon icon={star}>
                                            </IonIcon> {rating}
                                        </div>
                                    </IonCol>
                                </IonRow>
                            </IonCol>
                            <IonCol className="vadhyar-item-choose-place-add-btn" size="3" sizeSm="3"
                                    sizeLg="3">
                                <IonButton color="vadhyar" size="default" fill="clear">
                                    <IonIcon slot="icon-only" icon={add}/>
                                </IonButton>
                            </IonCol>
                        </IonRow>
                    </IonCardContent>
                </IonCard>
            </IonCol>
        </IonRow>
    );
};

export default VadhyarItemBookPlace;
