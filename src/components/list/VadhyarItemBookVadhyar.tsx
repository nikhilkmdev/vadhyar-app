import React from 'react';
import './VadhyarItemBookVadhyar.css';
import {IonButton, IonCard, IonCardContent, IonCol, IonIcon, IonRow} from "@ionic/react";
import {add, star} from "ionicons/icons";

interface ContainerProps {
    index: number;
    name: string;
    bio: string;
    experience: string;
    rating: number;
}

const VadhyarItemBookVadhyar: React.FC<ContainerProps> = ({index, name, bio, experience, rating}) => {

    return (
        <IonRow className="vadhyar-item-choose-vadhyar-row">
            <IonCol sizeLg="10">
                <IonCard className="vadhyar-item-choose-vadhyar-card">
                    <IonCardContent className="vadhyar-item-choose-vadhyar-card-content">
                        <IonRow>
                            <IonCol size="2" sizeSm="2" sizeLg="2"
                                    className="vadhyar-item-choose-vadhyar-index-col">
                                <div className="vadhyar-item-choose-vadhyar-index-text">{index}</div>
                            </IonCol>
                            <IonCol size="7" sizeSm="7" sizeLg="7">
                                <IonRow>
                                    <IonCol>
                                        <div className="vadhyar-item-choose-vadhyar-name">{name}</div>
                                    </IonCol>
                                </IonRow>
                                <IonRow>
                                    <IonCol>
                                        <div className="vadhyar-item-choose-vadhyar-bio">{bio}</div>
                                    </IonCol>
                                </IonRow>
                                <IonRow>
                                    <IonCol>
                                        <div
                                            className="vadhyar-item-choose-vadhyar-experience">{experience}
                                        </div>
                                    </IonCol>
                                    <IonCol>
                                        <div className="vadhyar-item-choose-vadhyar-rating">
                                            <IonIcon icon={star}>
                                            </IonIcon> {rating}
                                        </div>
                                    </IonCol>
                                </IonRow>
                            </IonCol>
                            <IonCol className="vadhyar-item-choose-vadhyar-add-btn" size="3" sizeSm="3"
                                    sizeLg="3">
                                <IonButton color="vadhyar" size="default" fill="clear">
                                    <IonIcon slot="icon-only" icon={add}/>
                                </IonButton>
                            </IonCol>
                        </IonRow>
                    </IonCardContent>
                </IonCard>
            </IonCol>
        </IonRow>
    );
};

export default VadhyarItemBookVadhyar;
