import React from 'react';
import './VadhyarItemActivity.css';
import {IonButton, IonCard, IonCardContent, IonCol, IonIcon, IonRow} from "@ionic/react";
import {add} from "ionicons/icons";

interface ContainerProps {
    index: number;
    name: string;
    description: string;
}

const VadhyarItemActivity: React.FC<ContainerProps> = ({index, name, description}) => {

    return (
        <IonRow className="vadhyar-item-activity-row">
            <IonCol sizeLg="10">
                <IonCard className="vadhyar-item-activity-card">
                    <IonCardContent className="vadhyar-item-activity-card-content">
                        <IonRow>
                            <IonCol size="2" sizeSm="2" sizeLg="2" className="vadhyar-item-activity-index-col">
                                <div className="vadhyar-item-activity-index-text">{index}</div>
                            </IonCol>
                            <IonCol size="7" sizeSm="7" sizeLg="7">
                                <IonRow>
                                    <IonCol>
                                        <div className="vadhyar-item-activity-name">{name}</div>
                                        <div className="vadhyar-item-activity-description">{description}</div>
                                    </IonCol>
                                </IonRow>
                            </IonCol>
                            <IonCol className="vadhyar-item-activity-add-btn" size="3" sizeSm="3" sizeLg="3">
                                <IonButton color="vadhyar" size="default" fill="clear">
                                    <IonIcon slot="icon-only" icon={add}/>
                                </IonButton>
                            </IonCol>
                        </IonRow>
                    </IonCardContent>
                </IonCard>
            </IonCol>
        </IonRow>
    );
};

export default VadhyarItemActivity;
