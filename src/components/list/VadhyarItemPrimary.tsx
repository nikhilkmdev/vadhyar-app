import React from 'react';
import './VadhyarItemPrimary.css';
import {IonCol, IonIcon, IonRow} from "@ionic/react";
import {chevronForward} from "ionicons/icons";

interface ContainerProps {
    icon: any;
    isFirst?: boolean;
    name: string;
}

const VadhyarItemPrimary: React.FC<ContainerProps> = ({icon, name, isFirst}) => {

    return (
        <IonRow className="vadhyar-item-primary-row">
            <IonCol
                className={isFirst ? "vadhyar-item-primary-icon-first" : "vadhyar-item-primary-icon"}
                size="3"
                sizeSm="4"
                sizeLg="2">
                <IonIcon icon={icon} size="large" color="light">
                </IonIcon>
            </IonCol>
            <IonCol className="vadhyar-item-primary-name" size="7" sizeSm="6" sizeLg="9">
                <div>{name}</div>
            </IonCol>
            <IonCol className="vadhyar-item-primary-chevron" size="2" sizeSm="2" sizeLg="1">
                <IonIcon icon={chevronForward} color="vadhyar">
                </IonIcon>
            </IonCol>
        </IonRow>
    );
};

export default VadhyarItemPrimary;
