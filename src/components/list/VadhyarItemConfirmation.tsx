import React from 'react';
import './VadhyarItemConfirmation.css';
import {IonCard, IonCardContent, IonCol, IonIcon, IonRow} from "@ionic/react";

interface ContainerProps {
    icon: any;
    name: string;
    cost: string;
    rating: number;
}

const VadhyarItemConfirmation: React.FC<ContainerProps> = ({icon, name, cost}) => {

    return (
        <IonRow className="vadhyar-item-confirmation-row">
            <IonCol sizeLg="10">
                <IonCard className="vadhyar-item-confirmation-card">
                    <IonCardContent className="vadhyar-item-confirmation-card-content">
                        <IonRow>
                            <IonCol size="3" sizeSm="3" sizeLg="3" className="vadhyar-item-confirmation-index-col">
                                <IonIcon icon={icon} size="large" color="vadhyar"/>
                            </IonCol>
                            <IonCol size="9" sizeSm="9" sizeLg="9">
                                <IonRow>
                                    <IonCol>
                                        <div className="vadhyar-item-confirmation-name">{name}</div>
                                    </IonCol>
                                </IonRow>
                                <IonRow>
                                    <IonCol>
                                        <div className="vadhyar-item-confirmation-cost">
                                            {cost}
                                        </div>
                                    </IonCol>
                                </IonRow>
                            </IonCol>
                        </IonRow>
                    </IonCardContent>
                </IonCard>
            </IonCol>
        </IonRow>
    );
};

export default VadhyarItemConfirmation;
