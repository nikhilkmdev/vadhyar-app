import React from 'react';
import './VadhyarItemBookCatering.css';
import {IonButton, IonCard, IonCardContent, IonCol, IonIcon, IonRow} from "@ionic/react";
import {add, star} from "ionicons/icons";

interface ContainerProps {
    index: number;
    name: string;
    bio: string;
    experience: string;
    rating: number;
}

const VadhyarItemBookCatering: React.FC<ContainerProps> = ({index, name, bio, experience, rating}) => {

    return (
        <IonRow className="vadhyar-item-choose-caterer-row">
            <IonCol sizeLg="10">
                <IonCard className="vadhyar-item-choose-caterer-card">
                    <IonCardContent className="vadhyar-item-choose-caterer-card-content">
                        <IonRow>
                            <IonCol size="2" sizeSm="2" sizeLg="2"
                                    className="vadhyar-item-choose-caterer-index-col">
                                <div className="vadhyar-item-choose-caterer-index-text">{index}</div>
                            </IonCol>
                            <IonCol size="7" sizeSm="7" sizeLg="7">
                                <IonRow>
                                    <IonCol>
                                        <div className="vadhyar-item-choose-caterer-name">{name}</div>
                                    </IonCol>
                                </IonRow>
                                <IonRow>
                                    <IonCol>
                                        <div className="vadhyar-item-choose-caterer-bio">{bio}</div>
                                    </IonCol>
                                </IonRow>
                                <IonRow>
                                    <IonCol>
                                        <div
                                            className="vadhyar-item-choose-caterer-experience">{experience}
                                        </div>
                                    </IonCol>
                                    <IonCol>
                                        <div className="vadhyar-item-choose-caterer-rating">
                                            <IonIcon icon={star}>
                                            </IonIcon> {rating}
                                        </div>
                                    </IonCol>
                                </IonRow>
                            </IonCol>
                            <IonCol className="vadhyar-item-choose-caterer-add-btn" size="3" sizeSm="3"
                                    sizeLg="3">
                                <IonButton color="vadhyar" size="default" fill="clear">
                                    <IonIcon slot="icon-only" icon={add}/>
                                </IonButton>
                            </IonCol>
                        </IonRow>
                    </IonCardContent>
                </IonCard>
            </IonCol>
        </IonRow>
    );
};

export default VadhyarItemBookCatering;
