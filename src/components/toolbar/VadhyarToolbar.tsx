import React from 'react';
import './VadhyarToolbar.css';
import {IonButton, IonButtons, IonIcon, IonTitle, IonToolbar} from "@ionic/react";
import {menu} from "ionicons/icons";

interface ContainerProps {
    icon?: any;
    onClick?: any;
    name: string;
}

const VadhyarToolbar: React.FC<ContainerProps> = ({name, icon, onClick}) => {

    return (
        <IonToolbar className="vadhyar-tool-bar">
            <IonButtons slot="start">
                <IonIcon icon={icon} color="vadhyar" size="medium"/>
            </IonButtons>
            <IonTitle className="vadhyar-tool-bar-title" size="small" color="vadhyar">{name}</IonTitle>
            <IonButtons slot="primary">
                <IonButton color="vadhyar">
                    <IonIcon slot="end" icon={menu}/>
                </IonButton>
            </IonButtons>
        </IonToolbar>
    );
};

export default VadhyarToolbar;

