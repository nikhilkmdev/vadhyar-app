import React from 'react';
import './VadhyarSelect.css';
import {IonItem, IonSelect, IonSelectOption} from "@ionic/react";

interface ContainerProps {
    label: string;
    selectAction: any;
    curValue: string;
    values: string[];
}

const VadhyarSelect: React.FC<ContainerProps> = ({label, values, curValue, selectAction}) => {

    return (
        <IonItem className="vadhyar-select-item">
            <IonSelect
                interface="popover"
                value={curValue}
                placeholder={label}
                onIonChange={(e) => selectAction(e.detail.value)}>
                {values.map((optionVal) => {
                    return <IonSelectOption value={optionVal}>{optionVal}</IonSelectOption>;
                })}
            </IonSelect>
        </IonItem>
    );
};

export default VadhyarSelect;

