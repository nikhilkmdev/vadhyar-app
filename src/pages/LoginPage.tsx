import React, {useState} from 'react';
import {IonButton, IonCol, IonContent, IonGrid, IonIcon, IonInput, IonItem, IonPage, IonRow} from '@ionic/react';
import './LoginPage.css';
import {chevronBack, chevronForward} from "ionicons/icons";
import {useHistory} from "react-router-dom";

interface ContainerProps {
    match: any;
}

const LoginPage: React.FC<ContainerProps> = ({match}) => {
    const isLogin = match.params.isLogin === "true";
    const history = useHistory();
    const [email, setEmail] = useState<string>();
    const [password, setPassword] = useState<string>();

    const login = () => {
        history.push('/home');
    };

    return (
        <IonPage>
            <IonContent fullscreen>
                <IonGrid className="login-grid">
                    <IonRow className="login-app-name-row">
                        <IonCol className="login-app-name-col">
                            <div className="login-app-name">
                                Vadhyar
                            </div>
                        </IonCol>
                    </IonRow>
                    <IonRow className="login-area-row">
                        <IonCol className="welcome-text-col">
                            <IonRow>
                                <IonCol>
                                    <IonItem color="vadhyar">
                                        <IonInput
                                            value={email}
                                            placeholder="Enter email - example@gmail.com"
                                            type="email"
                                            onIonChange={e => setEmail(e.detail.value!)}
                                            color="light">
                                        </IonInput>
                                    </IonItem>
                                </IonCol>
                            </IonRow>
                            <IonRow>
                                <IonCol>
                                    <IonItem color="vadhyar">
                                        <IonInput
                                            value={password}
                                            placeholder="Enter password"
                                            type="password"
                                            onIonChange={e => setPassword(e.detail.value!)}
                                            color="light">
                                        </IonInput>
                                    </IonItem>
                                </IonCol>
                            </IonRow>
                            <IonRow>
                                <IonCol>
                                    <IonButton
                                        expand="block"
                                        fill="outline"
                                        color="light"
                                        size="small"
                                        onClick={() => login()}>
                                        {isLogin ? "login" : "register"}
                                        <IonIcon slot="end" icon={chevronForward}/>
                                    </IonButton>
                                </IonCol>
                            </IonRow>
                            <IonRow>
                                <IonCol>
                                    <IonButton
                                        expand="block"
                                        fill="clear"
                                        color="light"
                                        size="small"
                                        onClick={() => login()}>
                                        <IonIcon slot="start" icon={chevronBack}/>
                                        back
                                    </IonButton>
                                </IonCol>
                            </IonRow>
                        </IonCol>
                    </IonRow>
                </IonGrid>
            </IonContent>
        </IonPage>
    );
};

export default LoginPage;
