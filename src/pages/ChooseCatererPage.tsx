import React from 'react';
import {IonContent, IonFab, IonFabButton, IonIcon, IonPage} from '@ionic/react';
import './ChooseCatererPage.css';
import {useHistory} from "react-router-dom";
import {arrowBackCircle, arrowForwardCircle} from "ionicons/icons";
import VadhyarItemBookCatering from "../components/list/VadhyarItemBookCatering";

interface ContainerProps {
    match: any;
}

const ChooseCatererPage: React.FC<ContainerProps> = ({match}) => {
    const isBackHome = match.params.isBackHome === "true";
    let history = useHistory();

    const goBack = () => {
        if (isBackHome) {
            history.push('/home');
        } else {
            history.push('/choose-vadhyar');
        }
    }

    const goToChoosePlace = () => {
        history.push('/choose-place');
    }

    return (
        <IonPage>
            <IonContent fullscreen className="vadhyar-choose-caterer-bg">
                <div className="vadhyar-choose-caterer-title">Book Caterer</div>
                <VadhyarItemBookCatering
                    index={1}
                    name="Ramesh Caterers"
                    rating={4.5}
                    bio="We have been serving people and in catering business for..."
                    experience="15 years">
                </VadhyarItemBookCatering>
                <VadhyarItemBookCatering
                    index={2}
                    name="Raghu Caterers"
                    rating={4.5}
                    bio="We have been serving people and in catering business for..."
                    experience="8 years">
                </VadhyarItemBookCatering>
                <IonFab vertical="bottom" horizontal="start" slot="fixed">
                    <IonFabButton onClick={() => goBack()} color="danger" size="small">
                        <IonIcon color="light" icon={arrowBackCircle}/>
                    </IonFabButton>
                </IonFab>
                <IonFab vertical="bottom" horizontal="end" slot="fixed">
                    <IonFabButton onClick={() => goToChoosePlace()} color="vadhyar" size="small">
                        <IonIcon icon={arrowForwardCircle}/>
                    </IonFabButton>
                </IonFab>
            </IonContent>
        </IonPage>
    );
};

export default ChooseCatererPage;
