import React from 'react';
import {IonContent, IonFab, IonFabButton, IonIcon, IonPage} from '@ionic/react';
import './ChoosePlacePage.css';
import {useHistory} from "react-router-dom";
import {arrowBackCircle, arrowForwardCircle} from "ionicons/icons";
import VadhyarItemBookPlace from "../components/list/VadhyarItemBookPlace";

interface ContainerProps {
    match: any;
}


const ChoosePlacePage: React.FC<ContainerProps> = ({match}) => {
    const isBackHome = match.params.isBackHome === "true";
    let history = useHistory();

    const goBack = () => {
        if (isBackHome) {
            history.push('/home')
        } else {
            history.push('/choose-caterer');
        }
    }

    const goToConfirmation = () => {
        history.push('/confirm-booking');
    }

    return (
        <IonPage>
            <IonContent fullscreen className="vadhyar-choose-place-bg">
                <div className="vadhyar-choose-place-title">Book Place</div>
                <VadhyarItemBookPlace
                    index={1}
                    name="Andal Shram"
                    rating={4.5}
                    bio="We have been serving people and in catering business for..."
                    cost="15,000 INR">
                </VadhyarItemBookPlace>
                <VadhyarItemBookPlace
                    index={2}
                    name="Dandusa"
                    rating={4.5}
                    bio="We have been serving people and in catering business for..."
                    cost="15,000 INR">
                </VadhyarItemBookPlace>
                <IonFab vertical="bottom" horizontal="start" slot="fixed">
                    <IonFabButton onClick={() => goBack()} color="danger" size="small">
                        <IonIcon color="light" icon={arrowBackCircle}/>
                    </IonFabButton>
                </IonFab>
                <IonFab vertical="bottom" horizontal="end" slot="fixed">
                    <IonFabButton onClick={() => goToConfirmation()} color="vadhyar" size="small">
                        <IonIcon icon={arrowForwardCircle}/>
                    </IonFabButton>
                </IonFab>
            </IonContent>
        </IonPage>
    );
};

export default ChoosePlacePage;
