import React from 'react';
import {IonContent, IonFab, IonFabButton, IonIcon, IonPage} from '@ionic/react';
import './ConfirmationPage.css';
import {useHistory} from "react-router-dom";
import {arrowBackCircle, busSharp, checkmark, flameSharp, restaurantSharp, storefrontSharp} from "ionicons/icons";
import VadhyarItemConfirmation from "../components/list/VadhyarItemConfirmation";

const ConfirmationPage: React.FC = () => {
    let history = useHistory();

    const goToChoosePlace = () => {
        history.push('/choose-place');
    }

    const goHome = () => {
        history.push('/home');
    }

    return (
        <IonPage>
            <IonContent fullscreen className="vadhyar-confirmation-bg">
                <div className="vadhyar-confirmation-title">Event Name</div>
                <VadhyarItemConfirmation
                    icon={flameSharp}
                    name="Srinivas Raghavan"
                    rating={4.5}
                    cost="15,000 INR">
                </VadhyarItemConfirmation>
                <VadhyarItemConfirmation
                    icon={restaurantSharp}
                    name="Ramesh Caterers"
                    rating={4.5}
                    cost="50,000 INR">
                </VadhyarItemConfirmation>
                <VadhyarItemConfirmation
                    icon={storefrontSharp}
                    name="Dandusa"
                    rating={4.5}
                    cost="25,000 INR">
                </VadhyarItemConfirmation>
                {/*<VadhyarItemConfirmation*/}
                {/*    icon={busSharp}*/}
                {/*    name="Jothi Travels"*/}
                {/*    rating={4.5}*/}
                {/*    cost="15,000 INR">*/}
                {/*</VadhyarItemConfirmation>*/}
                <IonFab vertical="bottom" horizontal="start" slot="fixed">
                    <IonFabButton onClick={() => goToChoosePlace()} color="danger" size="small">
                        <IonIcon color="light" icon={arrowBackCircle}/>
                    </IonFabButton>
                </IonFab>
                <IonFab vertical="bottom" horizontal="end" slot="fixed">
                    <IonFabButton onClick={() => goHome()} color="success" size="small">
                        <IonIcon icon={checkmark}/>
                    </IonFabButton>
                </IonFab>
            </IonContent>
        </IonPage>
    );
};

export default ConfirmationPage;
