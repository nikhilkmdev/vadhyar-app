import React from 'react';
import {IonButton, IonCol, IonContent, IonGrid, IonIcon, IonPage, IonRow} from '@ionic/react';
import './WelcomePage.css';
import {chevronForward} from "ionicons/icons";

const WelcomePage: React.FC = () => {

    return (
        <IonPage>
            <IonContent fullscreen>
                <IonGrid className="welcome-grid">
                    <IonRow className="welcome-app-name-row">
                        <IonCol className="welcome-app-name-col">
                            <div className="welcome-app-name">
                                Vadhyar
                            </div>
                        </IonCol>
                    </IonRow>
                    <IonRow className="welcome-area-row">
                        <IonCol className="welcome-text-col">
                            <div className="welcome-text">
                                welcome!!
                            </div>
                        </IonCol>
                    </IonRow>
                    <IonRow className="welcome-area-button-row">
                        <IonCol>
                            <IonButton
                                expand="block"
                                fill="clear"
                                color="light"
                                size="small"
                                routerLink="/login/true">
                                login
                                <IonIcon slot="end" icon={chevronForward}/>
                            </IonButton>
                        </IonCol>
                        <IonCol>
                            <IonButton
                                expand="block"
                                fill="clear"
                                color="light"
                                size="small"
                                routerLink="/login/false">
                                register
                                <IonIcon slot="end" icon={chevronForward}/>
                            </IonButton>
                        </IonCol>
                    </IonRow>
                </IonGrid>
            </IonContent>
        </IonPage>
    );
};

export default WelcomePage;
