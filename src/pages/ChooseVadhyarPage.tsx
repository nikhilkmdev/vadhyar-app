import React from 'react';
import {IonContent, IonFab, IonFabButton, IonIcon, IonPage} from '@ionic/react';
import './ChooseVadhyarPage.css';
import {useHistory} from "react-router-dom";
import {arrowBackCircle, arrowForwardCircle} from "ionicons/icons";
import VadhyarItemBookVadhyar from "../components/list/VadhyarItemBookVadhyar";

interface ContainerProps {
    match: any;
}

const ChooseVadhyarPage: React.FC<ContainerProps> = ({match}) => {
    const isBackHome = match.params.isBackHome === "true";
    let history = useHistory();

    const goBack = () => {
        console.log(isBackHome);
        if (isBackHome) {
            history.push('/home');
        } else {
            history.push('/choose-activity');
        }
    }

    const goToChooseCaterer = () => {
        history.push('/choose-caterer');
    }

    return (
        <IonPage>
            <IonContent fullscreen className="vadhyar-choose-vadhyar-bg">
                <div className="vadhyar-choose-vadhyar-title">Book Vadhyar</div>
                <VadhyarItemBookVadhyar
                    index={1}
                    name="Srinivas Raghavan"
                    rating={4.5}
                    bio="Have been performing rituals and customs since.."
                    experience="15 years">
                </VadhyarItemBookVadhyar>
                <VadhyarItemBookVadhyar
                    index={2}
                    name="Anant Krishna"
                    rating={4.5}
                    bio="Have been performing rituals and customs since.."
                    experience="8 years">
                </VadhyarItemBookVadhyar>
                <VadhyarItemBookVadhyar
                    index={3}
                    name="Vasu"
                    rating={4.5}
                    bio="Have been performing rituals and customs since.."
                    experience="5 years">
                </VadhyarItemBookVadhyar>
                <VadhyarItemBookVadhyar
                    index={4}
                    name="Sheenu"
                    rating={4.5}
                    bio="Have been performing rituals and customs since.."
                    experience="5 years">
                </VadhyarItemBookVadhyar>
                <VadhyarItemBookVadhyar
                    index={5}
                    name="Bugga"
                    rating={4.5}
                    bio="Have been performing rituals and customs since.."
                    experience="5 years">
                </VadhyarItemBookVadhyar>
                <IonFab vertical="bottom" horizontal="start" slot="fixed">
                    <IonFabButton onClick={() => goBack()} color="danger" size="small">
                        <IonIcon color="light" icon={arrowBackCircle}/>
                    </IonFabButton>
                </IonFab>
                <IonFab vertical="bottom" horizontal="end" slot="fixed">
                    <IonFabButton onClick={() => goToChooseCaterer()} color="vadhyar" size="small">
                        <IonIcon icon={arrowForwardCircle}/>
                    </IonFabButton>
                </IonFab>
            </IonContent>
        </IonPage>
    );
};

export default ChooseVadhyarPage;
