import React from 'react';
import {IonContent, IonFab, IonFabButton, IonIcon, IonPage} from '@ionic/react';
import './ChooseActivityPage.css';
import {useHistory} from "react-router-dom";
import VadhyarItemActivity from "../components/list/VadhyarItemActivity";
import {arrowBackCircle, arrowForwardCircle} from "ionicons/icons";

const ChooseActivityPage: React.FC = () => {
    let history = useHistory();

    const goBack = () => {
        history.push('/book-event');
    }

    const goToVadhyar = () => {
        history.push('/choose-vadhyar');
    }

    return (
        <IonPage>
            <IonContent fullscreen className="vadhyar-choose-vadhyar-bg">
                <div className="vadhyar-choose-vadhyar-title">Activities</div>
                <VadhyarItemActivity
                    index={1}
                    name="Vishnu Pooja"
                    description="Devote your time in service for lord vishnu.. Is part of every function brahmins do">
                </VadhyarItemActivity>
                <VadhyarItemActivity
                    index={2}
                    name="Ram Pooja"
                    description="Devote your time in service for lord ram.. Is part of every function brahmins do">
                </VadhyarItemActivity>
                <VadhyarItemActivity
                    index={3}
                    name="Catering"
                    description="Food needs time as well right? Let's dig in">
                </VadhyarItemActivity>
                <VadhyarItemActivity
                    index={4}
                    name="Vishnu Pooja"
                    description="Devote your time in service for lord vishnu.. Is part of every function brahmins do">
                </VadhyarItemActivity>
                <VadhyarItemActivity
                    index={5}
                    name="Vishnu Pooja"
                    description="Devote your time in service for lord vishnu.. Is part of every function brahmins do">
                </VadhyarItemActivity>
                <IonFab vertical="bottom" horizontal="start" slot="fixed">
                    <IonFabButton onClick={() => goBack()} color="danger" size="small">
                        <IonIcon color="light" icon={arrowBackCircle}/>
                    </IonFabButton>
                </IonFab>
                <IonFab vertical="bottom" horizontal="end" slot="fixed">
                    <IonFabButton onClick={() => goToVadhyar()} color="vadhyar" size="small">
                        <IonIcon icon={arrowForwardCircle}/>
                    </IonFabButton>
                </IonFab>
            </IonContent>
        </IonPage>
    );
};

export default ChooseActivityPage;
