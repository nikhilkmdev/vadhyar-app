import React, {useState} from 'react';
import {
    IonCol,
    IonContent,
    IonDatetime,
    IonFab,
    IonFabButton,
    IonIcon,
    IonInput,
    IonItem,
    IonList,
    IonPage,
    IonRow
} from '@ionic/react';
import {arrowForwardCircle, flameSharp} from 'ionicons/icons';
import './EventBookingPage.css';
import VadhyarToolbar from "../components/toolbar/VadhyarToolbar";
import {useHistory} from "react-router-dom";
import VadhyarSelect from "../components/select/VadhyarSelect";

const BookEventPage: React.FC = () => {
    let history = useHistory();
    const [eventName, setEventName] = useState('');
    const [eventDate, setEventDate] = useState('');
    const [sect, setSect] = useState('');
    const [sects, setSects] = useState([
        'Brahmin',
        'Madhwa',
    ]);
    const [event, setEvent] = useState('');
    const [events, setEvents] = useState([
        'Kalyanam',
        'Punal',
        'Shamasharnam',
        'Shanti',
    ]);

    const goToChooseActivity = () => {
        history.push('/choose-activity');
    }

    return (
        <IonPage>
            <VadhyarToolbar name="Create Event" icon={flameSharp}>
            </VadhyarToolbar>
            <IonContent fullscreen>
                <IonList className="screen-list">
                    <IonRow className="vadhyar-book-item-row">
                        <IonCol
                            className="vadhyar-book-item-icon-first"
                            size="3"
                            sizeSm="4"
                            sizeLg="2">
                            <div className="vadhyar-book-item-index">{1}.</div>
                        </IonCol>
                        <IonCol className="vadhyar-book-item-col">
                            <VadhyarSelect
                                label="Select Sect"
                                selectAction={setSect}
                                curValue={sect}
                                values={sects}>
                            </VadhyarSelect>
                        </IonCol>
                    </IonRow>
                    <IonRow className="vadhyar-book-item-row">
                        <IonCol
                            className="vadhyar-book-item-icon"
                            size="3"
                            sizeSm="4"
                            sizeLg="2">
                            <div className="vadhyar-book-item-index">{2}.</div>
                        </IonCol>
                        <IonCol className="vadhyar-book-item-col">
                            <VadhyarSelect
                                label="Select Event"
                                selectAction={setEvent} curValue={event}
                                values={events}>
                            </VadhyarSelect>
                        </IonCol>
                    </IonRow>
                    <IonRow>
                        <IonCol
                            className="vadhyar-book-item-icon"
                            size="3"
                            sizeSm="4"
                            sizeLg="2">
                            <div className="vadhyar-book-item-index">{3}.</div>
                        </IonCol>
                        <IonCol className="vadhyar-book-item-col">
                            <IonItem className="vadhyar-input-item">
                                <IonInput
                                    value={eventName}
                                    placeholder="Type Event Name"
                                    type="email"
                                    onIonChange={e => setEventName(e.detail.value!)}
                                    color="vadhyar">
                                </IonInput>
                            </IonItem>
                        </IonCol>
                    </IonRow>
                    <IonRow>
                        <IonCol
                            className="vadhyar-book-item-icon"
                            size="3"
                            sizeSm="4"
                            sizeLg="2">
                            <div className="vadhyar-book-item-index">{4}.</div>
                        </IonCol>
                        <IonCol className="vadhyar-book-item-col">
                            <IonItem className="vadhyar-input-item">
                                <IonDatetime
                                    displayFormat="DD MMM YYYY"
                                    placeholder="Event Date"
                                    value={eventDate}
                                    onIonChange={e => setEventDate(e.detail.value!)}>
                                </IonDatetime>
                            </IonItem>
                        </IonCol>
                    </IonRow>
                </IonList>
                <IonFab vertical="bottom" horizontal="end" slot="fixed">
                    <IonFabButton onClick={() => goToChooseActivity()} color="vadhyar" size="small">
                        <IonIcon icon={arrowForwardCircle}/>
                    </IonFabButton>
                </IonFab>
            </IonContent>
        </IonPage>
    );
};

export default BookEventPage;
