import React from 'react';
import {IonContent, IonList, IonPage} from '@ionic/react';
import {calendarClearSharp, flameSharp, restaurantSharp, storefrontSharp} from 'ionicons/icons';
import './HomePage.css';
import VadhyarItemPrimary from "../components/list/VadhyarItemPrimary";
import VadhyarToolbar from "../components/toolbar/VadhyarToolbar";
import {useHistory} from "react-router-dom";

const HomePage: React.FC = () => {
    let history = useHistory();

    return (
        <IonPage>
            <VadhyarToolbar name="Vadhyar" icon={flameSharp}>
            </VadhyarToolbar>
            <IonContent fullscreen>
                <IonList className="screen-list">
                    <div onClick={() => history.push('/book-event')}>
                        <VadhyarItemPrimary
                            icon={calendarClearSharp}
                            name="Manage Event"
                            isFirst={true}>
                        </VadhyarItemPrimary>
                    </div>
                    <div onClick={() => history.push('/choose-vadhyar/true')}>
                        <VadhyarItemPrimary
                            icon={flameSharp}
                            name="Vadhyar">
                        </VadhyarItemPrimary>
                    </div>
                    <div onClick={() => history.push('/choose-caterer/true')}>
                        <VadhyarItemPrimary icon={restaurantSharp} name="Catering">
                        </VadhyarItemPrimary>
                    </div>
                    <div onClick={() => history.push('/choose-place/true')}>
                        <VadhyarItemPrimary icon={storefrontSharp} name="Place">
                        </VadhyarItemPrimary>
                    </div>
                </IonList>
            </IonContent>
        </IonPage>
    );
};

export default HomePage;
